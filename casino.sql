-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 16, 2018 at 10:48 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casino`
--

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(4) NOT NULL,
  `title` varchar(255) NOT NULL,
  `const` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `title`, `const`, `created_at`, `updated_at`) VALUES
(1, 'Рубль', 'RUB', '2018-09-15 08:17:16', '2018-09-15 08:17:16'),
(2, 'Евро', 'EURO', '2018-09-15 08:17:00', '2018-09-15 08:17:44'),
(3, 'Доллар', 'DOLLAR', '2018-09-15 08:18:00', '2018-09-15 08:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9),
(23, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(24, 4, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 8),
(25, 4, 'type_id', 'text', 'Type Id', 0, 1, 1, 1, 1, 1, NULL, 5),
(26, 4, 'img_url', 'image', 'Img Url', 0, 1, 1, 1, 1, 1, NULL, 9),
(27, 4, 'sum', 'number', 'Сумма', 0, 1, 1, 1, 1, 1, NULL, 10),
(28, 4, 'currency_id', 'text', 'Currency Id', 0, 1, 1, 1, 1, 1, NULL, 7),
(29, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 12),
(30, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(31, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 2),
(33, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(34, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(35, 4, 'prize_belongsto_currency_relationship', 'relationship', 'Валюта', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"currency_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"currencies\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(36, 4, 'prize_belongsto_prize_type_relationship', 'relationship', 'Тип', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\PrizeType\",\"table\":\"prize_types\",\"type\":\"belongsTo\",\"column\":\"type_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"currencies\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(37, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(38, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 2),
(39, 6, 'const', 'text', 'Const', 1, 1, 1, 1, 1, 1, NULL, 3),
(40, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(41, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(42, 4, 'prize_belongsto_user_relationship', 'relationship', 'Пользователь', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"currencies\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(43, 4, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(44, 4, 'completed', 'checkbox', 'Отправлено', 0, 1, 1, 1, 1, 1, '{\"on\":\"Да\",\"off\":\"Нет\",\"checked\":true}', 11);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(4, 'prizes', 'prizes', 'Приз', 'Призы', 'voyager-medal-rank-star', 'App\\Prize', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-15 08:06:13', '2018-09-15 10:20:22'),
(5, 'prize_types', 'prize-types', 'Тип', 'Типы', NULL, 'App\\PrizeType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-15 08:10:31', '2018-09-15 08:10:31'),
(6, 'currencies', 'currencies', 'Валюта', 'Валюты', 'voyager-dollar', 'App\\Currency', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-15 08:16:42', '2018-09-15 08:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-09-15 07:59:28', '2018-09-15 07:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 3, '2018-09-15 07:59:28', '2018-09-15 08:16:55', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 6, '2018-09-15 07:59:28', '2018-09-15 08:16:55', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 5, '2018-09-15 07:59:28', '2018-09-15 08:16:55', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 4, '2018-09-15 07:59:28', '2018-09-15 08:16:55', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 7, '2018-09-15 07:59:28', '2018-09-15 08:16:55', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-09-15 07:59:28', '2018-09-15 08:08:19', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-09-15 07:59:28', '2018-09-15 08:08:19', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-09-15 07:59:28', '2018-09-15 08:08:19', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-09-15 07:59:28', '2018-09-15 08:08:19', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 8, '2018-09-15 07:59:28', '2018-09-15 08:16:55', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-09-15 07:59:28', '2018-09-15 08:08:19', 'voyager.hooks', NULL),
(12, 1, 'Призы', '', '_self', 'voyager-medal-rank-star', '#000000', NULL, 1, '2018-09-15 08:06:13', '2018-09-15 08:09:08', NULL, 'null'),
(13, 1, 'Список', '', '_self', NULL, '#000000', 12, 1, '2018-09-15 08:08:50', '2018-09-15 08:08:57', 'voyager.prizes.index', NULL),
(14, 1, 'Типы', '', '_self', NULL, NULL, 12, 2, '2018-09-15 08:10:31', '2018-09-15 08:10:45', 'voyager.prize-types.index', NULL),
(15, 1, 'Валюты', '', '_self', 'voyager-dollar', NULL, NULL, 2, '2018-09-15 08:16:42', '2018-09-15 08:16:55', 'voyager.currencies.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(2, 'browse_bread', NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(3, 'browse_database', NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(4, 'browse_media', NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(5, 'browse_compass', NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(6, 'browse_menus', 'menus', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(7, 'read_menus', 'menus', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(8, 'edit_menus', 'menus', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(9, 'add_menus', 'menus', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(10, 'delete_menus', 'menus', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(11, 'browse_roles', 'roles', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(12, 'read_roles', 'roles', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(13, 'edit_roles', 'roles', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(14, 'add_roles', 'roles', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(15, 'delete_roles', 'roles', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(16, 'browse_users', 'users', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(17, 'read_users', 'users', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(18, 'edit_users', 'users', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(19, 'add_users', 'users', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(20, 'delete_users', 'users', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(21, 'browse_settings', 'settings', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(22, 'read_settings', 'settings', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(23, 'edit_settings', 'settings', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(24, 'add_settings', 'settings', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(25, 'delete_settings', 'settings', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(26, 'browse_hooks', NULL, '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(27, 'browse_prizes', 'prizes', '2018-09-15 08:06:13', '2018-09-15 08:06:13'),
(28, 'read_prizes', 'prizes', '2018-09-15 08:06:13', '2018-09-15 08:06:13'),
(29, 'edit_prizes', 'prizes', '2018-09-15 08:06:13', '2018-09-15 08:06:13'),
(30, 'add_prizes', 'prizes', '2018-09-15 08:06:13', '2018-09-15 08:06:13'),
(31, 'delete_prizes', 'prizes', '2018-09-15 08:06:13', '2018-09-15 08:06:13'),
(32, 'browse_prize_types', 'prize_types', '2018-09-15 08:10:31', '2018-09-15 08:10:31'),
(33, 'read_prize_types', 'prize_types', '2018-09-15 08:10:31', '2018-09-15 08:10:31'),
(34, 'edit_prize_types', 'prize_types', '2018-09-15 08:10:31', '2018-09-15 08:10:31'),
(35, 'add_prize_types', 'prize_types', '2018-09-15 08:10:31', '2018-09-15 08:10:31'),
(36, 'delete_prize_types', 'prize_types', '2018-09-15 08:10:31', '2018-09-15 08:10:31'),
(37, 'browse_currencies', 'currencies', '2018-09-15 08:16:42', '2018-09-15 08:16:42'),
(38, 'read_currencies', 'currencies', '2018-09-15 08:16:42', '2018-09-15 08:16:42'),
(39, 'edit_currencies', 'currencies', '2018-09-15 08:16:42', '2018-09-15 08:16:42'),
(40, 'add_currencies', 'currencies', '2018-09-15 08:16:42', '2018-09-15 08:16:42'),
(41, 'delete_currencies', 'currencies', '2018-09-15 08:16:42', '2018-09-15 08:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prizes`
--

DROP TABLE IF EXISTS `prizes`;
CREATE TABLE `prizes` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type_id` int(2) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `sum` float(10,2) DEFAULT NULL,
  `currency_id` int(4) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prizes`
--

INSERT INTO `prizes` (`id`, `title`, `user_id`, `type_id`, `img_url`, `sum`, `currency_id`, `completed`, `created_at`, `updated_at`) VALUES
(1, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(2, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(3, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(4, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(5, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(6, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(7, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(8, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(9, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(10, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(11, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(12, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:40:13'),
(13, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(14, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(15, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(16, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(17, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(18, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(19, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(20, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(21, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(22, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(23, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(24, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(25, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(26, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(27, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(28, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(29, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(30, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(31, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(32, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(33, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(34, 'Кружка нашего казино', 1, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:31:05'),
(35, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(36, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(37, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(38, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(39, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(40, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(41, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(42, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(43, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(44, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(45, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(46, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(47, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(48, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(49, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(50, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(51, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(52, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(53, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(54, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:03:06'),
(55, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(56, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(57, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(58, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(59, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(60, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(61, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(62, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(63, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(64, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(65, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(66, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(67, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(68, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(69, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(70, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(71, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(72, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(73, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(74, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(75, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(76, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:03:49'),
(77, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(78, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(79, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(80, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(81, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(82, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(83, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(84, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(85, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(86, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(87, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(88, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(89, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:06:05'),
(90, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(91, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(92, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(93, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:06:36'),
(94, 'Вам бонус', 1, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:31:08'),
(95, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(96, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(97, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(98, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(99, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(100, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(101, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(102, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(103, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(104, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(105, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(106, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(107, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(108, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(109, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(110, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(111, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(112, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:08:00'),
(113, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(114, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(115, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(116, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(117, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(118, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(119, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(120, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(121, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(122, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(123, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(124, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(125, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(126, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(127, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(128, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(129, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(130, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(131, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(132, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(133, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(134, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(135, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(136, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(137, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(138, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(139, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(140, 'Вам бонус', 1, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:31:11'),
(141, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(142, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(143, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(144, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(145, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(146, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(147, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(148, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(149, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(150, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(151, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(152, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:11:34'),
(153, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:12:20'),
(154, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(155, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(156, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(157, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(158, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(159, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(160, 'Вам бонус', 1, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:31:03'),
(161, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(162, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(163, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(164, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(165, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(166, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(167, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(168, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(169, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(170, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:12:45'),
(171, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(172, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:08:24'),
(173, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(174, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(175, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(176, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(177, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(178, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(179, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(180, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(181, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(182, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(183, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(184, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(185, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(186, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(187, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:08:35'),
(188, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:13:30'),
(189, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(190, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(191, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:09:35'),
(192, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(193, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(194, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(195, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:25'),
(196, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(197, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:09:50'),
(198, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(199, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(200, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(201, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(202, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(203, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(204, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(205, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(206, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:28'),
(207, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(208, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(209, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(210, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(211, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(212, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(213, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:29'),
(214, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(215, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(216, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(217, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:30'),
(218, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(219, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(220, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(221, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:30'),
(222, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:31'),
(223, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(224, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:32'),
(225, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(226, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(227, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(228, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(229, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:32'),
(230, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(231, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-16 04:16:33'),
(232, 'Вам бонус', NULL, 2, 'prizes/September2018/92laOsEmBFsyZLJTHRIi.png', 1000.00, NULL, 0, '2018-09-15 08:25:00', '2018-09-15 08:34:08'),
(233, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(234, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(235, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(236, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(237, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:09:05'),
(238, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(239, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:09:13'),
(240, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(241, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(242, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:26'),
(243, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(244, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:14'),
(245, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:15'),
(246, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(247, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(248, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(249, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 13:45:09'),
(250, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:09:19'),
(251, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(252, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(253, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(254, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:16'),
(255, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(256, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(257, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:34:19'),
(258, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(259, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(260, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(261, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(262, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(263, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(264, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(265, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(266, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(267, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:16'),
(268, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(269, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(270, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(271, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:09:24'),
(272, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(273, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(274, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(275, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:16'),
(276, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:08:51'),
(277, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:17'),
(278, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:17'),
(279, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(280, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(281, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(282, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:18'),
(283, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(284, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:19'),
(285, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(286, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(287, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(288, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(289, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:20'),
(290, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(291, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(292, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:21'),
(293, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(294, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:30:24'),
(295, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(296, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-16 04:34:54'),
(297, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(298, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(299, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(300, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(301, 'Выигрыш', NULL, 1, 'prizes/September2018/uy5WMvXglTpzE0NOSrp6.png', 1500.00, 1, 0, '2018-09-15 08:23:00', '2018-09-15 08:35:05'),
(302, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:16:58'),
(303, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(304, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:16:59'),
(305, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(306, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(307, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(308, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(309, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(310, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(311, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(312, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(313, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:00'),
(314, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:00'),
(315, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(316, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:01'),
(317, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:01'),
(318, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(319, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(320, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:02'),
(321, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(322, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(323, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:02'),
(324, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(325, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:03'),
(326, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(327, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:03'),
(328, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(329, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(330, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(331, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:05'),
(332, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(333, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:17:08'),
(334, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-15 12:57:28'),
(335, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:30:29'),
(336, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:30:29'),
(337, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:30:30'),
(338, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:30:30'),
(339, 'Кружка нашего казино', NULL, 3, 'prizes/September2018/vPaw1qlGhqFd2rwKxwSQ.png', NULL, NULL, 1, '2018-09-15 08:32:00', '2018-09-16 04:30:31');

-- --------------------------------------------------------

--
-- Table structure for table `prize_types`
--

DROP TABLE IF EXISTS `prize_types`;
CREATE TABLE `prize_types` (
  `id` int(4) NOT NULL,
  `title` varchar(255) NOT NULL,
  `const` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prize_types`
--

INSERT INTO `prize_types` (`id`, `title`, `const`, `created_at`, `updated_at`) VALUES
(1, 'Денежный', 'MONEY', '2018-09-15 10:49:55', '2018-09-15 16:26:24'),
(2, 'Бонусные баллы', 'POINTS', '2018-09-15 10:50:37', '2018-09-15 16:26:24'),
(3, 'Физический предмет', 'SUBJECT', '2018-09-15 10:50:37', '2018-09-15 16:26:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-09-15 07:59:28', '2018-09-15 07:59:28'),
(2, 'user', 'User', '2018-09-15 07:59:28', '2018-09-15 07:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `details` text COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@email.com', 'users/default.png', '$2y$10$zSOkhEPsOGO3ft89iKSJ6u8mlKOcaxWRDhAb0q44cPreq1CY/cKl.', 'yTPudaxgbKtbU6rQHG9flU3j7FPJVVn1tnbAMOJwsdElwS9zSOhxs5HSU5BA', '{\"locale\":\"ru\"}', '2018-09-15 08:00:41', '2018-09-15 08:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `user_bills`
--

DROP TABLE IF EXISTS `user_bills`;
CREATE TABLE `user_bills` (
  `id` int(32) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `currency_id` int(4) NOT NULL DEFAULT '1',
  `sum` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_bills`
--

INSERT INTO `user_bills` (`id`, `user_id`, `currency_id`, `sum`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0.00, '2018-09-15 13:35:24', '2018-09-16 04:34:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_points`
--

DROP TABLE IF EXISTS `user_points`;
CREATE TABLE `user_points` (
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `value` int(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_points`
--

INSERT INTO `user_points` (`user_id`, `value`, `created_at`, `updated_at`, `id`) VALUES
(1, 5250, '2018-09-15 13:39:52', '2018-09-16 04:34:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `prizes`
--
ALTER TABLE `prizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prizes_currencies_id_fk` (`currency_id`),
  ADD KEY `prizes_prize_types_id_fk` (`type_id`),
  ADD KEY `prizes_users_id_fk` (`user_id`);

--
-- Indexes for table `prize_types`
--
ALTER TABLE `prize_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_bills`
--
ALTER TABLE `user_bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bills_users_id_fk` (`user_id`),
  ADD KEY `bills_currencies_id_fk` (`currency_id`);

--
-- Indexes for table `user_points`
--
ALTER TABLE `user_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_points_users_id_fk` (`user_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `prizes`
--
ALTER TABLE `prizes`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- AUTO_INCREMENT for table `prize_types`
--
ALTER TABLE `prize_types`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_bills`
--
ALTER TABLE `user_bills`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_points`
--
ALTER TABLE `user_points`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `prizes`
--
ALTER TABLE `prizes`
  ADD CONSTRAINT `prizes_currencies_id_fk` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prizes_prize_types_id_fk` FOREIGN KEY (`type_id`) REFERENCES `prize_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prizes_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_bills`
--
ALTER TABLE `user_bills`
  ADD CONSTRAINT `bills_currencies_id_fk` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `bills_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_points`
--
ALTER TABLE `user_points`
  ADD CONSTRAINT `user_points_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
