@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    @if($items->count() > 0)
                        <div class="card-body container-fluid">
                            <div class="alert alert-warning" style="display: none" role="alert"></div>
                            <ul class="list-group">
                                @foreach ($items as $k => $v)
                                    <li class="list-group-item" data-id="{{ $v->id }}">
                                        <div class="col-md-12 d-inline-flex">
                                            <div class="col-md-8">
                                                <img src="/storage/{{ $v->img_url }}"
                                                     style="width: 40px"><span>{{ $v->title }}</span>
                                            </div>
                                            <div class="col-md-4 d-inline-flex">
                                                @if ($v->type->const == 'MONEY')
                                                    <button onclick="convertToPoints({{ $v->id }})" type="button"
                                                            class="btn btn-primary">{{ _('В баллы') }}</button>
                                                @endif
                                                <button onclick="deletePrize({{ $v->id }})" type="button"
                                                        class="btn btn-danger">{{ _('Отказаться') }}</button>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsScripts')
    <script type="text/javascript">

        /**
         * Delete prize
         * @param id
         */
        function deletePrize(id) {

            var url = '/quiz/remove/' + id;

            $.ajax(url, {
                method: 'GET',
                success: function (data) {
                    $('.alert').html(data.message).show();
                    $('li[data-id=' + id + ']').remove();
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var data = $.parseJSON(xhr.responseText);
                    console.log(data);
                }
            });
        }

        function convertToPoints(id) {
            var url = '/quiz/convert-to-points/' + id;

            $.ajax(url, {
                method: 'GET',
                success: function (data) {
                    $('.alert').html(data.message).show();
                    $('li[data-id=' + id + ']').remove();
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var data = $.parseJSON(xhr.responseText);
                    console.log(data);
                }
            });
        }
    </script>
@endsection