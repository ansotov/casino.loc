@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    @if($prize)
                        <div class="card-body" style="text-align: center">


                            {{ $prize->id }}
                            <div class="alert alert-warning" style="display: none" role="alert"></div>

                            <h3>{{ _('Поздравляем!') }}<br>{{ _('Ваш приз') }}: {{ $prize->title }}
                                - {{ $prize->type->title }}</h3>
                            <div style="margin: 20px 0"><a class="btn btn-success" id="savePrize"
                                                           href="javascript:void(0);">{{ _('Забрать') }}</a></div>
                            <img width="40%" src="/storage/{{ $prize->img_url }}">
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsScripts')
    <script type="text/javascript">
        $(function () {
            $('#savePrize').on('click', function (e) {
                e.preventDefault();
                var url = '/quiz/save/{{ $prize->id }}';

                $.ajax(url, {
                    dataType: "json",
                    success: function (data) {

                        if (data.status == 'error') {
                            $('.alert').html(data.message).show();
                        } else {
                            $('.card-body').html(data.message);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var data = $.parseJSON(xhr.responseText);
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endsection