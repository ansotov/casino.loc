<?php

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');

    return "Кэш очищен.";
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'quiz'], function () {
        Route::get('/', 'QuizController@getPrize')->name('quiz.prize');
        Route::get('/my', 'QuizController@getMyPrizes')->name('quiz.my-prizes');
        Route::get('/convert-to-points/{id}', 'QuizController@convertToPoints')->name('quiz.convert-to-points');
        Route::get('/save/{id}', 'QuizController@setPrize')->name('quiz.save');
        Route::get('/remove/{id}', 'QuizController@removePrize')->name('quiz.remove');
    });
});