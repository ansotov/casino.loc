<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    /**
     * Get data type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\PrizeType', 'type_id', 'id');
    }

}
