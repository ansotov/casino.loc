<?php

namespace App\Http\Controllers;

use App\Prize;
use App\PrizeType;
use App\UserBill;
use App\UserPoint;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuizController extends Controller
{
    private $item;
    private $items = [];
    private $typeIds = [];

    //Settings. It's for example, need Settings table in database
    private $maxMoney = 100000;
    private $maxSubjects = 100;
    private $pointEquivalent = 1.5;

    /**
     * Get random prize
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPrize()
    {
        try {

            $this->getTypes();

            // In random data I can't using order by RAND() if is it highload project
            $this->item = Prize::where(['prizes.user_id' => NULL])
                ->join(DB::raw('( SELECT FLOOR(RAND() * (SELECT MAX(id) FROM prizes WHERE user_id IS NULL AND type_id IN(' . implode(',', $this->typeIds) . '))) AS max_id ) AS m'), function ($join) {
                    $join->on("prizes.id", ">=", "m.max_id");
                })
                ->whereIn('type_id', $this->typeIds)
                ->first();

            return view('prize',
                [
                    'prize' => $this->item
                ]
            );
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Get auth user prizes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMyPrizes()
    {
        $this->items = Prize::where(['user_id' => Auth::user()->id])->get();


        return view('my-prizes',
            [
                'items' => $this->items
            ]
        );
    }

    /**
     * Save prize
     * @param $id
     * @return JsonResponse
     */
    public function setPrize($id)
    {
        try {
            if ($this->item = Prize::where(['id' => $id, 'user_id' => null])->first()) {
                $this->item->user_id = Auth::user()->id;
                $this->item->save();

                $this->setDataPrize($this->item);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Подарок получен, поздравляем!.'
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Такого подарка не существует.'
                ]);
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Remove prize
     * @param $id
     * @return JsonResponse|string
     */
    public function removePrize($id)
    {
        try {
            if ($this->item = Prize::where(['id' => $id, 'user_id' => Auth::user()->id])->first()) {
                $this->item->user_id = NULL;
                $this->item->save();

                switch ($this->item->type->const) {
                    case 'MONEY':
                        $item = UserBill::where(['user_id' => Auth::user()->id, 'currency_id' => $this->item->currency_id])->first();
                        $item->sum -= $this->item->sum;
                        $item->save();
                        break;
                    case 'POINTS':
                        $item = UserPoint::where(['user_id' => Auth::user()->id])->first();
                        $item->value -= $this->item->sum;
                        $item->save();
                        break;
                    case 'SUBJECT':
                        // Do something
                        break;
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Вы отказались от подарка.'
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Такого подарка не существует.'
                ]);
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function convertToPoints($id)
    {
        try {
            $this->item = Prize::where(['id' => (int)$id, 'user_id' => Auth::user()->id])->first();

            $userPoints = UserPoint::where(['user_id' => Auth::user()->id])->first();
            $userPoints->value += ($this->item->sum * $this->pointEquivalent);
            $userPoints->save();

            // Remove user prize form user's prizes
            $this->removePrize($id);

            return response()->json([
                'status' => 'success',
                'message' => 'Деньги успешно конвертированы в баллы.'
            ]);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Set data by type
     * @param Prize $prize
     */
    private function setDataPrize(Prize $prize)
    {
        switch ($prize->type->const) {
            case 'MONEY':
                $this->item = UserBill::firstOrCreate(['user_id' => Auth::user()->id, 'currency_id' => $prize->currency_id]);
                $this->item->sum += $prize->sum;
                $this->item->save();

                // Send money to user bill
                $this->billing($prize);

                break;
            case 'POINTS':
                $this->item = UserPoint::firstOrCreate(['user_id' => Auth::user()->id]);
                $this->item->value += $prize->sum;
                $this->item->save();
                break;
            case 'SUBJECT':
                // Do something
                break;
        }
    }

    /**
     * Get types for random query by max items from settings
     */
    private function getTypes()
    {
        foreach (PrizeType::all() as $k => $v) {
            if ($v->const == 'SUBJECT') {
                $count = Prize::where(['user_id' => Auth::user()->id, 'type_id' => $v->id])->count();

                if ($count < $this->maxSubjects) {
                    $this->typeIds[] = $v->id;
                }
            } elseif ($v->const == 'MONEY') {
                $sum = Prize::where(['user_id' => Auth::user()->id, 'type_id' => $v->id])->sum('sum');

                if ($sum < $this->maxMoney) {
                    $this->typeIds[] = $v->id;
                }
            } else {
                $this->typeIds[] = $v->id;
            }
        }
    }

    /**
     * Send money to user's bank bill
     * @param Prize $prize
     */
    private function billing(Prize $prize)
    {
        // Send API request to user's bill of bank
    }
}
